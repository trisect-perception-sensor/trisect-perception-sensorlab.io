#!/bin/sh
#
# Convenience script which merges the "staging" branch into 
# the "published branch"
#
git checkout published
git merge staging

git push --all origin

git checkout staging