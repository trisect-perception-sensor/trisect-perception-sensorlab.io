---

Contains a simple redirect from [https://trisect-perception-sensor.gitlab.io](https://trisect-perception-sensor.gitlab.io/) to [trisect-docs](https://trisect-perception-sensor.gitlab.io/trisect-docs/).

